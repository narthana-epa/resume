Narthana Epa's Résumé
===

This repository houses the LaTeX source code and build pipeline for my résumé. The source is forked from [Deedy's Template](https://github.com/deedy/Deedy-Resume) but I have modified it slightly.

To view the latest version of the résumé, you may download it from the permalink: <https://gitlab.com/triarius/resume/-/releases/permalink/latest/downloads/resume.pdf>.

Alternatively, you may download and compile it with a LaTeX distribution that includes XeLaTeX. TeXLive and MacTeX should work, though only one of them is likely to be tested for any given version of the résumé.
